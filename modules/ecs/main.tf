#####
# ECS cluster and fargate
#####
resource "aws_ecs_cluster" "cluster" {
  name = var.name
  capacity_providers = ["FARGATE_SPOT", "FARGATE"]

  default_capacity_provider_strategy {
    capacity_provider = "FARGATE"
  }

  setting {
    name  = "containerInsights"
    value = "enabled"
  }
}

module "fargate" {
  source = "umotif-public/ecs-fargate/aws"
  version = "~> 6.1.0"

  name_prefix = "${var.name}-nginx"

  vpc_id             =  var.vpc_id
  private_subnet_ids =  var.subnets
  cluster_id         = aws_ecs_cluster.cluster.id

  wait_for_steady_state = true

  platform_version = "1.4.0"

  task_container_image   = "nginx:1.20.1"
  task_definition_cpu    = 256
  task_definition_memory = 512

  task_container_port             = 80
  task_container_assign_public_ip = true

  target_groups = [
    {
      target_group_name = "${var.name}-nginx"
      container_port    = 80
    }
  ]

  health_check = {
    port = "traffic-port"
    path = "/"
  }

  task_stop_timeout = 90

}

resource "aws_security_group_rule" "task_ingress_80" {
  security_group_id        = module.fargate.service_sg_id
  type                     = "ingress"
  protocol                 = "tcp"
  from_port                = 80
  to_port                  = 80
  source_security_group_id = var.alb_security_group_id
}

resource "aws_lb_listener" "alb_80" {
  load_balancer_arn = var.alb_arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = module.fargate.target_group_arn[0]
  }
}

