variable "name" {
  type = string
  description = "ecs cluster name"
}
variable "vpc_id" {
  type = string
  description = "VPC for container definition deployment"
}

variable "subnets" {
  type = list(string)
  description = "deployment subnets"
}

variable "alb_security_group_id" {
  type = string
  description = "ALB security group id"
}

variable "alb_arn" {
  type = string
  description = "arn of application load balancer"
}