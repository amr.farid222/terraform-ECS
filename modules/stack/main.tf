### AWs config
provider "aws" {
  region = var.region 
}

### module to create VPC and sub resources
module "vpc" {
  source = "../vpc/"
  vpc_cidr = var.vpc_cidr
  name = var.environment_name
}

### module to create the LoadBalancer
module "alb" {
  source = "../alb/"
  vpc_id = module.vpc.vpc_id
  name_prefix = var.environment_name
  subnets = module.vpc.public_subnets
  route53_zone_id = var.route53_zone_id
}

module "ecs" {
  source = "../ecs/"
  alb_arn = module.alb.alb_arn
  alb_security_group_id = module.alb.alb_security_group_id
  name = var.environment_name
  subnets = module.vpc.private_subnets
  vpc_id = module.vpc.vpc_id

}