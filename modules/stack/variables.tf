variable "vpc_cidr" {
 description = "CIDR block for new VPC"
 type = string
}

variable "environment_name" {
 description = "The environment name"
 type = string
}

variable "region" {
 description = "Region in which the stack should be deployed"
 type = string
}

variable "tags" {
 description = "Tags to be added to all resoruces"
 type = map(string)
}

variable "route53_zone_id" {
 description = "route53 zone id to create a record for alb in it"
 type = string
}