locals {
  zones = slice(data.aws_availability_zones.this.names, 0 , 3)
  subnets = cidrsubnets(var.vpc_cidr, 8, 8, 8, 8, 8, 8)
}

module "vpc" {
  source = "terraform-aws-modules/vpc/aws"
  version = "v3.10.0"

  name = var.name
  cidr = var.vpc_cidr

  azs             = local.zones
  private_subnets = slice(local.subnets, 0, 3)
  public_subnets  = slice(local.subnets, 3, 6)
  enable_nat_gateway = true
  single_nat_gateway     = true
  one_nat_gateway_per_az = false

  tags = var.tags
}

