variable "name_prefix" {
  description = "Name prefix for the load balancer"
  type = string
}

variable "vpc_id" {
  description = "VCP id in which the loadbalacer should be deployed in"
  type = string
}

variable "subnets" {
  description = "LoadBalancer subnets"
  type = list(string)
}

variable "route53_zone_id" {
  description = "route53 zone id to create a record for alb in it"
  type = string
}