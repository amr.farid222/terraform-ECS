### Load Balancer
module "alb" {
  source  = "terraform-aws-modules/alb/aws"
  version = "~> 6.0"
  depends_on = [aws_s3_bucket_policy.this]
  name_prefix = var.name_prefix
  load_balancer_type = "application"
  vpc_id             = var.vpc_id
  subnets            = var.subnets
  security_groups    = [aws_security_group.this.id]
  access_logs = {
    bucket = aws_s3_bucket.this.bucket
  }
}

resource "aws_s3_bucket_policy" "this" {
  bucket = aws_s3_bucket.this.id
  policy = data.aws_iam_policy_document.this.json
}


resource "aws_s3_bucket" "this" {
  bucket = "${var.name_prefix}-alb-bucket-logs"
}

resource "aws_security_group" "this" {
  vpc_id =  var.vpc_id
}


#####
# Security Group Config
#####
resource "aws_security_group_rule" "alb_ingress_80" {
  security_group_id = aws_security_group.this.id
  type              = "ingress"
  protocol          = "tcp"
  from_port         = 80
  to_port           = 80
  cidr_blocks       = ["0.0.0.0/0"]
  ipv6_cidr_blocks  = ["::/0"]
}

resource "aws_security_group_rule" "alb_egress_80" {
  security_group_id = aws_security_group.this.id
  type              = "egress"
  protocol          = "tcp"
  from_port         = 0
  to_port           = 65535
  cidr_blocks       = ["0.0.0.0/0"]
  ipv6_cidr_blocks  = ["::/0"]
}

resource "aws_route53_health_check" "this" {
  fqdn              = module.alb.lb_dns_name
  port              = 80
  type              = "HTTP"
  resource_path     = "/"
  failure_threshold = "5"
  request_interval  = "30"
}


resource "aws_route53_record" "this" {
  zone_id = var.route53_zone_id
  name    = "www"
  type    = "CNAME"
  ttl     = "5"
  set_identifier = "${var.name_prefix}-healthcheck-alb"
  health_check_id = aws_route53_health_check.this.id
  latency_routing_policy {
    region = data.aws_region.this.name
  }
  records = [
    module.alb.lb_dns_name
  ]
}

