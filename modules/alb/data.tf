data "aws_elb_service_account" "this" {}

data "aws_iam_policy_document" "this" {
  policy_id = "s3_bucket_lb_logs"
  statement {
    actions = [
      "s3:PutObject",
    ]
    effect = "Allow"
    resources = [
      "${aws_s3_bucket.this.arn}/*",
    ]

    principals {
      identifiers = [data.aws_elb_service_account.this.arn]
      type        = "AWS"
    }
  }

}

data "aws_region" "this" {}