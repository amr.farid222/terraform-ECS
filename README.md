# Terraform ECS 

### Description 

Terraform module to create simple nginx deployment


#### How to create a new stack 
- Create a new directory under `environments` folder with any name
- Create a new main.tf file wiht this content
```hcl
locals {
  vpc_cidr = "10.2.0.0/16"
  environment_name = "env4"
  region = "us-east-1"
  route53_zone_id= "Z0746547DLJ9MN43OP0A"
  tags = {}
}

### module to create VPC and sub resources
module "stack" {
  source = "../../modules/stack/"
  environment_name = local.environment_name
  region = "eu-west-1"
  vpc_cidr = local.vpc_cidr
  tags = local.tags
  route53_zone_id = local.route53_zone_id
}

output "alb_dns_name" {
  value = module.stack.alb_dns_name
}
```
- apply the new stack
```shell
terraform init
terraform validate 
terraform plan
terraform apply
```

#### Testing
- Curl to the alb dns name from the output of the previous command 
```shell
curl <alb-dns-name>
```

#### Thoughts 
- [ ] Create ACM certificate and configure alb to use it 
- [ ] Deploy cloudfront distribution for static content caching
- [ ] Deploy WAF to rate limiting and prevent DDOS attacks
- [ ] Implement autoscaling for nginx deployment based on CPU resources or custom metrics (number of pending requests)
- [ ] Create a transit gateway to allow traffic between different stack if access to shared resources is required
- [ ] Add unit test for terraform modules using terratest
- [ ] Static code analysis using conftest, trivy, terrascan
- [ ] Scan deployed images on the cluster using ECR scanning service or trivy
- [ ] Create a gitlab pipeline to deploy new stacks
- [ ] Create another module for cloudwatch alerts to send alerts based on some metrics, for example
  - 5xx, 4xx request rate
  - number of healthy targets
  - Average latency time
  - And some alerts based on application logs using cloudwatch logs service
  
- Terraform state to be on s3 and using dynamodb for locking. 
- Integrate VPC module with IPAM system like [netbox](https://netbox.readthedocs.io/en/stable/) to automatically get new CIDR 
  
        
  



