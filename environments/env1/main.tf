locals {
  vpc_cidr = "10.2.0.0/16"
  environment_name = "env4"
  region = "us-east-1"
  route53_zone_id= "Z0746547DLJ9MN43OP0A"
  tags = {}
}

### module to create VPC and sub resources
module "stack" {
  source = "../../modules/stack/"
  environment_name = local.environment_name
  region = "eu-west-1"
  vpc_cidr = local.vpc_cidr
  tags = local.tags
  route53_zone_id = local.route53_zone_id
}

output "alb_dns_name" {
  value = module.stack.alb_dns_name
}